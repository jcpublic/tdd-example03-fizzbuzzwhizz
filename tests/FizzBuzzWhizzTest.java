import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FizzBuzzWhizzTest {

	FizzBuzzWhizz fizzApp;
	@Before
	public void setUp() throws Exception {
		fizzApp = new FizzBuzzWhizz();
	}

	@After
	public void tearDown() throws Exception {
	}

	// R0 - Function accepts number greater than 0
	@Test
	public void testFunctionAcceptsNumberGreaterThanZero() {
		assertEquals("error",  fizzApp.fbw(-1));
	}
	
	// R1: Return "fizz" if the number is divisible by 3
	@Test
	public void testNumberIsDivisibleBy3() {
		int n = 6;
		assertEquals("fizz",  fizzApp.fbw(n));
	}
	
	// R2: Return "buzz" if the number is divisible by 5
	@Test
	public void testNumberIsDivisibleBy55() {
		int n = 10;
		assertEquals("buzz",  fizzApp.fbw(n));
	}

	
	// R3: Return "fizzbuzz" if the number is divisible by 3 or 5 (15)
	@Test
	public void testNumberIsDivisibleBy3or5() {
		int n = 15;
		assertEquals("fizzbuzz",  fizzApp.fbw(n));
	}
	
	// R4: Return the number if no other requirement is fulfilled. 
	// The numbers must be returned as a string.
	@Test
	public void testNumberDoesNotMeetOtherConditions() {
		int n = 4;
		assertEquals("4",  fizzApp.fbw(n));
	}
	
	// R5: If number is prime, return “whizz” 
	@Test
	public void testNumberIsPrime() {
		// Changed input to 101 because 3 & 5 are covered by another test case
		int n = 101;
		assertEquals("whizz",  fizzApp.fbw(n));
	}
	
	// R6:
	// If number meets (R5) AND (R1, R2, or R3) 
	// - append “whizz” to end of string
	// A1:  R5 + R1  input = 3, output = fizzwhizz
	// A2:  R5 + R2  input = 5, output = buzzwhizz
	// A3:  R5 + R3	 input = IMPOSSIBLE !!! NO INPUTS
	//  - We can ignore A3, because:
	//  - There is no NUMBER that meets R5 + R3 (Prime + Divisible by 15)
	@Test
	public void testNumberMeetsR5R1R3() {
		int n = 3;
		assertEquals("fizzwhizz",  fizzApp.fbw(n));
		
		n = 5;
		assertEquals("buzzwhizz", fizzApp.fbw(n));
		
		n = 7;
		assertEquals("whizz", fizzApp.fbw(n));
	} 

	
	
	
	
	
	
	
	
}
