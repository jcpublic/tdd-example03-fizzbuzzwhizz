
public class FizzBuzzWhizz {

	public FizzBuzzWhizz() {
		// empty constructor - we will not use this
	}

	public String fbw(int n) {
		if (n <= 0) {
			return "error";
		} 
		
		// check if number is prime
		boolean isPrime = checkIsPrime(n);
		if (isPrime == true) {
			if (n % 3 == 0) {
				return "fizzwhizz";
			}
			else if (n % 5 == 0) {
				return "buzzwhizz";
			}
			return "whizz";
		}
		
		// start your checks for divisbility
		if (n % 3 == 0 && n % 5 == 0) {
			// check if its divisible by 3 & 5
			// before you check other cases
			return "fizzbuzz";
		}
		else if (n % 3 == 0) {
			return "fizz";
		}
		else if (n % 5 == 0) {
			return "buzz";
		}


		return String.valueOf(n);

		
	}
	
	// Refactor - put prime checking code into a helper function
	public boolean checkIsPrime(int n) {
		// check if number is prime
		boolean isPrime = true;
		for (int i = 2; i < n; i++) {
			if (n % i == 0) {
				isPrime = false;
				break;
			}
		}
		
		return isPrime;
	}
	
}
